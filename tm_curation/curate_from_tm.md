# BioKC Training: Curate facts from a literature collection

This excercise will cover:
- upload of an example literature collection to BioKC
- building a simple fact using selected entries
- building an advanced fact

## Read first: vocabulary and data model

BioKC relies on [SBML (Systems Biology Markup Language)](https://sbml.org) as its underlying data model and follows [annotation qualifiers](https://github.com/combine-org/combine-specifications/blob/main/specifications/qualifiers.md) for its annotations. This influences the vocabulary of BioKC.  

Important vocabulary disambiguations:
- **compartment** a physical or conceptual location of elements of interactions
- **species** are elements of interactions; in SBML every species has an assigned  compartment
- **reactions** describe interactions between interacting elements: **reactants** and **products**
- **modifiers** are elements of a reaction (interaction) with a specific role; in SBML  a reaction can have an element that modifies them.

In SBML, a reaction (interaction) must have at least one reactant (input) and at least one product (output).

In SBML, a reaction can have multiple reactants, products and modifiers. Reaction type is the same for reactants and products. Each modifier can have a different role (type of modifying relationship).

## Table of contents
- [Literature collection](#literature-collection)
- [Step 1: Import sentences](#step-1-import-sentences-from-the-data-set)
- [Step 2: Curate from basket](#step-2-build-facts-from-entries-in-the-basket)

## Literature collection

For this excercise, we will use a set of ~37k entries transformed from a text mining of a literature corpus on immune mechanisms using [INDRA](https://indra.readthedocs.io/en/latest/).

The file can be [downloaded from here](https://webdav-r3lab.uni.lu/public/biocore/biokc/data/indra_biokc.tsv "download").

A smaller version, having only 1k entries can be [downloaded from here](https://webdav-r3lab.uni.lu/public/biocore/biokc/data/indra_biokc_small.tsv "download").

The file contains a table with the following columns:
- **id**: the identifier of an entry (PubMed ID)
- **codes**: stable identifiers of molecules in this entry, separated with an `|` symbol
- **literals**: names corresponding to the identifiers, separated with an `|` symbol
- **sentence**: a sentence with the evidence for this entry

## Step 1: Import sentences from the data set

### 0. Log in to [the BioKC](https://biokb.lcsb.uni.lu/login)

### 1.1. Open the import menu </font>

1.1.1 Use the options icon (![menuicon](../images/0_menu.svg)) to open the main menu 

1.1.2 Choose "Import sentences"

<img src="../images/tmc_1_1_login_menu.png"  width="600">

### 1.2. Import sentences from a file
    
1.2.1 Choose the "indra_biokc.tsv" file (1 in the figure below)

1.2.2 Explore sentences from the file are imported and shown in the area below (2 in the figure below)

<img src="../images/tmc_1_2_import.png"  width="600">

### 1.3. Filter by keywords
	
1.3.1 Type the keyword you want to filter entries by; this can be a publication identifier, molecule identifier or a term in the sentence. Example: use "tnf" as a keyword (1 in the figure below). 
Important to note:
- text based searches are less precise ("tnf" query will have more results than "P01375")
- multiple keywords can be combined wtih space as a separator (e.g. "TNF IKK") 

1.3.2 Notice that the number of results is dynamically reduced (2 in the figure below)

<img src="../images/tmc_1_3_filter_imported_sentences.png"  width="600">

### 1.4. Add entries to the "Curator's basket"
    
1.4.1 Select entries you want to curate; metadata is fetched for publication identifiers (1 in the figure below)

1.4.2 Notice that data from selected entries is added to the "Curator's basket" (2 in the figure below)

1.4.3 Click "Go to basket" to start curation using the selected entries (3 in the figure below)
	
<img src="../images/tmc_1_4_select_basket.png"  width="600">

## Step 2: Build facts from entries in the Basket

Take a look at the [vocabulary disclaimer](#read-first-vocabulary-and-data-model) to read about species (elements), compartments (location) and reactions (interactions).

### 2.1. Initial steps

2.1.1 In the "Curator's basket", notice the entries selected in the previous step (1 in the figure below)

2.1.2 Set the fact group to "Training"; this is where your facts will be saved (persisted) later (2 in the figure below)

2.1.3 Set up the initial compartment for the facts, use ![menuadd](../images/0_add.png) button to add a compartment and ![menuedit](../images/0_edit.png) to edit it. In the example below, the compartment is set to "cell" with the [qualifier annotation](http://mbine.org/standards/qualifiers) set to "bqbiol:is" (*identity, entity is identical to the subject of referenced resource*, [see here](https://github.com/combine-org/combine-specifications/blob/main/specifications/qualifiers.md)), annotation ontology set to "Cell Ontology" and the assigned identifier to [CL:0000000 (cell)](http://purl.obolibrary.org/obo/CL_0000000) (3 in the figure below).

2.1.4 Set the default qualifier to "bqbiol:is" and the annotation ontology set to "Uniprot Knowledgebase".
These will be used as defaults when adding elements.

<img src="../images/tmc_2_1_basket_details_inset.png"  width="600">

### 2.2. Add elements

2.2.1 Drag and drop elements of an interaction. Example: drag "TNF" and "p65".

<img src="../images/tmc_2_2_move.png"  width="600">

2.2.2 Cell type and element type need to be defined for the imported elements.  

Example: 
- use previolusly defined "cell" as the "Compartment"
- set "Bioentity type" to "Generic protein"
- identifier qualifier and namespace are set from defaults.  

Repeat for p65 and set its name to "RELA" by modifying the field "Species Name".

<img src="../images/tmc_2_3_complete.png"  width="400">

2.2.3 With defined elements, create an interaction: use ![menuadd](../images/0_add.png) button to add a compartment and ![menuedit](../images/0_edit.png) to edit it. Complete the information to create a simple interaction representing positive influence of TNF on RELA (p65).  

- use previolusly defined "cell" as the "Compartment"
- set "Reaction type" to "Positive influence"
- set "Reactant" to "TNF" and "Product" to "RELA".  

<img src="../images/tmc_2_4_reaction.png"  width="400">

2.2.4 With defined interaction, create a fact and annotate it with the sentence. Use ![menuadd](../images/0_add.png) button to add a fact. Then:

1. Drag created reaction to the reaction area of the fact.

2. Drag the corresponding field to the evidence area of the fact.

<img src="../images/tmc_2_5_fact_p1.png" width="600">

2.2.4 Persist a fact using the ![menusave](../images/0_save.png) button. Persisted facts will have an identifier assigned and all further edits to a persisted fact will be versioned. Please remember that currently your fact is visible only to the privileged members of your group. [Read about group management here](../configuration/configure.md)

<img src="../images/tmc_2_5_fact_p2.png"  width="600">