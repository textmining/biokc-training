# BioKC Training

This is a practical introduction to the [BioKC biocuration tool](https://biokc.pages.uni.lu).

For this tutorial, a user account is required, [please register](https://biokb.lcsb.uni.lu/register).

## Getting started

BioKC (Biological Knowledge Curation) is a tool for collaborative curation of knowledge into standardised format.  
A basic element of BioKC is a **fact**.

**BioKC fact** is composed of biomolecules, their interactions and annotations.  
Sentences from the literature provide evidence for the contents.

In BioKC, curators build facts, review them and can persist them as a publicly accessible resource.

Chrome is the recommended browser for BioKC and this training.

## Exercises

A set of excercises will guide the user through the following activities:

- [go here](./configuration/configure.md) to learn how to register and manage access to your facts
- [go here for](./tm_curation/curate_from_tm.md) curation of a simple and an advanced fact from a preprocessed literature collection (e.g. text mining results)
- [go here for](./denovo_curation/curate_from_scratch.md) for a tutorial on building a new fact from scratch
- [in preparation](./publish/version_and_publish.md) review of a fact
- [in preparation](./review/review.md) publishing and versioning a fact
