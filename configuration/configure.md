# BioKC Training: Register and manage your group

This excercise will cover:
- creating an account in BioKC
- editing user details
- browsing and configuring your group

## Read first

### The landing page

The landing page of BioKC is [https://biokb.lcsb.uni.lu](https://biokb.lcsb.uni.lu). This is not a typo - BioKB (Biological Knowledge **Base**) is the root project for BioKC (Biological Knowledge **Curation**) and the two share the landing page.

### BioKC groups

Facts curated in BioKC are organised into groups. By default, every registered user is a manager of their own group, to which they can invite others. In case you want to set up a group dedicated to a project, please contact the administrators (biokc@uni.lu).

## Table of contents
- [Step 1: Register yourself](#step-1-register)
- [Step 2: Edit user details](#step-2-edit-user-details)
- [Step 3: Edit group details](#step-3-edit-group-details)

## Step 1: Register

1.1 Go to the website [https://biokb.lcsb.uni.lu](https://biokb.lcsb.uni.lu), select

![login](../images/0_login.png)

then 

![login](../images/0_register.png)

1.2 Fill out the registration form

<img src="../images/umg_1_register.png"  width="400">

<br/><br/>

Athough not mandatory, it's highly recommended to provide your ORCID ([https://orcid.org](https://orcid.org)).  
This is especially important if you're planning to release your curated facts and reference their stable versions.  

After pressing "Submit" your account is set up!

## Step 2: Edit user details

When you're logged in, you can press  
![user](../images/0_user.png)  
button to access user management screen (upper right corner).

<img src="../images/umg_2_facts.png"  width="600">

<br/><br/>

The screen by default shows facts of a given user as a list. This user has 16 curated facts across different groups.

## Step 3: Edit group details

1. Click on the "Groups" icon, to  see the groups you belong to and access them.  
Here, our freshly registered Demo User (different than the user shown above) has access only to their own default group ("demo_user's Facts").

<img src="../images/umg_3_0_groups.png"  width="600">

<br/><br/>

2. Click on the group's name to see its contents and configure the group.  
Our Demo User is a manager of their group. Currently the group has no curated facts, and one member - Demo User. 

<img src="../images/umg_3_1_groups_facts.png"  width="600">

<br/><br/>

3. Click on "Edit group" to configure its details.

<img src="../images/umg_3_2_group_edit_p1.png"  width="600">

<br/><br/>

3. Change the name to "demo_user group" and add user "marek" to 
- "Readers" (users that can view facts)
- "Annotators" (users that can annotate facts) and 
- "Curators" (users that can curate/edit facts).  
Click on "Update group" to save your settings.  
Now your selected users can work with you on the facts in your group.  
The list of your changes is visible in the bar at the top of the screen.

<img src="../images/umg_3_2_group_edit_p2.png"  width="600">

<br/><br/>

4. Press "Back" see the details of the updated group. 

<img src="../images/umg_3_3_groups_facts_updated.png"  width="600">