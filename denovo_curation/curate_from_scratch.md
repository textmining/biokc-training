# BioKC Training: Curate a fact from scratch

This excercise will cover:
- steps needed to start curating a new fact
- the structure of a fact
- a curation of an example fact based on a scientific article


## Read first: vocabulary and data model

BioKC relies on [SBML (Systems Biology Markup Language)](https://sbml.org) as its underlying data model and follows [annotation qualifiers](https://github.com/combine-org/combine-specifications/blob/main/specifications/qualifiers.md) for its annotations. This influences the vocabulary of BioKC.  

Important vocabulary disambiguations:
- **model** equivalent to a fact; an entirety of knowledge on species, their interactions and compartments, including annotations and literature references
- **compartment** a physical or conceptual location of elements of interactions
- **species** are elements of interactions; in SBML every species has an assigned  compartment
- **reactions** describe interactions between interacting elements: **reactants** and **products**
- **modifiers** are elements of a reaction (interaction) with a specific role; in SBML  a reaction can have an element that modifies them.

In SBML, a reaction (interaction) must have at least one reactant (input) and at least one product (output).

In SBML, a reaction can have multiple reactants, products and modifiers. Reaction type is the same for reactants and products. Each modifier can have a different role (type of modifying relationship).

## Table of contents
- [Selected article](#selected-article)
- [Step 1: Create a fact](#step-1-create-a-fact)
- [Step 2: Curate a fact](#step-2-curate-a-fact)

## Selected article

For this excercise, we will use the following article:

Song P, et al, Protein Cell. 2016 Feb;7(2):114-29.  
*Parkin promotes proteasomal degradation of p62: implication of selective vulnerability of neuronal cells in the pathogenesis of Parkinson's disease.*  
[PMID: 26746706](https://pubmed.ncbi.nlm.nih.gov/26746706/).

Most notably the abstract of the article. We will extract the entities and their reported interactions to build a fact out of it.

## Step 1: Create a fact

### 0. Log in to [the BioKC](https://biokb.lcsb.uni.lu/login)

### 1.1. Open the "Add fact" menu

1.1.1 Use the options icon (![menuicon](../images/0_menu.svg)) to open the main menu.

1.1.2 Choose "Add fact" button.

<img src="../images/dnc_1_1_login_menu.png"  width="600">

### 1.2. Parameterise and start curation of a new fact
    
1.2.1 Select a group to which the fact will be assigned ([read about group management here](../configuration/configure.md)). Adapt the fact name and prefix if needed. Provide fact description.

1.2.2 Press "Start new fact" to create a fact and move to the dedicated curation space.

<img src="../images/dnc_1_2_login_create.png"  width="600">

## Step 2: Curate a fact

### 2.1. Examine the dedicated curation space

<img src="../images/dnc_2_1_start.png"  width="600">

2.1.1 A banner with a beginner's guide if this is a first fact created by this curator. The guide higlights key elements of the interface.

2.1.2 Visualisation area. Here your components of your fact will be shown. Currently only the root (fact name) is visible.

2.1.3 Version history. This is covered by a separate [trainig excercise](../publish/version_and_publish.md) and concerns already created facts.

2.1.4 Fact components (see also [vocabulary notes above](#read-first-vocabulary-and-data-model)). "Model" contains base informations about the "Fact".

2.1.5 Curator's toolbar allowing to start curation, provide sentences and assign tasks to other curators (review, annotate).

### 2.1. Start curating

2.1.1 Press "play button" ![menuplay](../images/0_start_curation.png) to start working with the fact. From the drop down menu choose "Curate Fact". Note that the symbol changed to ![menustop](../images/0_stop_curation.png).

2.1.2 Open the "Model" tab add the identifier of the article we will use to curate the fact:

- click on the "+" button in the "Identifiers"
- set "Qualifier" to "bqbiol:isDescribedBy" (*"entity is described by the referenced resource"*, [see here](https://github.com/combine-org/combine-specifications/blob/main/specifications/qualifiers.md)), "Namespace" to "PubMed" and "Resource" to [26746706](https://pubmed.ncbi.nlm.nih.gov/26746706/).
- click "Save model".

Note that more than multiple articles, or other resources like pathway databases, can be added as evidences to a single fact.

<img src="../images/dnc_2_2_model.png"  width="600">

### 2.2. Set up compartment and elements

2.2.1 First we need to add a compartment in which our curated interaction will take place. Open the "Compartments" tab and:
- click "Add compartment".
- set the "Compartment name" to "neuron".
- add an identifier.
- set set "Qualifier" to "bqbiol:is" (*identity, entity is identical to the subject of referenced resource*, [see here](https://github.com/combine-org/combine-specifications/blob/main/specifications/qualifiers.md)), "Namespace" to "Cell Type Ontology", and "Resource" to "[CL:0000540](https://bioportal.bioontology.org/ontologies/CL?p=classes&conceptid=CL%3A0000540)".
- Parameters "Spatial Dimensions", "Size" and "Constant" are SBML specific and are left as is.
- click "Add compartment".

Now the compartment is available in the "Select A Compartment" dropdown list, in case more annotations for it are needed.

2.2.2 Next, we need to add interacting elements. 
Before, please note that the article's title starts with "*Parkin promotes proteasomal degradation of p62*" in fact neither Parkin nor p62 are standard gene or protein identifiers. The first one is [PRKN](https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/HGNC:8607) in HGNC and [O60260](https://www.uniprot.org/uniprotkb/O60260/entry) in UniProt. The second is [SQSTM](https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/HGNC:11280) ([Q13501](https://www.uniprot.org/uniprotkb/Q13501/entry)). We will use these identifiers.

Open the "Species" tab and: 
- click "Add Species".
- set "Species Name" to its HGNC symbol "PRKN" and select "neuron" in the "Compartment" drop down list.
- for the "Bioentity Type" choose "Generic Protein".
- add the UniProt identifier by clicking the "+" in the "Identifiers" section, and use the following parameters: Qualifier: "bqbiol:is", Namespace: "UniProt Knowledgebase", Resource: "O60260".
- click "Add species".
- repeat the same for SQSTM1 (UniProt: Q13501)

**Important:**  
Let's take a look at the sentence from the abstract - "*Parkin directly interacts with and ubiquitinates p62 at the K13 to promote proteasomal degradation of p62 even in the absence of ATG5*".  
Having PRKN and SQSTM1 allows us to create a two element interaction representing "PRKN inhibits SQSTM1" (see [this training material](../tm_curation/curate_from_tm.md)).  
Here, let's create an interaction that reflects better the described biology.

2.2.3 Let's create a SQSTM1 version that is ubiquitinated on residue K13.
- click "Add species".
- Set the name to "SQSTM1 (K13 ubiquitinated)"
- set the compartment to "neuron", type to "Generic Protein" annotate it with UniProt Q13501.
- add another identifier, with
  - Qualifier "bqbiol:hasProperty" (*is a property of the biological entity represented by the model element*, [see here](https://github.com/combine-org/combine-specifications/blob/main/specifications/qualifiers.md))
  - Namespace "Protein Modification Ontology"
  - Resource "MOD:01148"

### 2.3. Create an interaction

2.3.1 Click on the "Reactions" tab, "Add Reaction"
- set "Reaction Type" to "State Transition"
- set "Compartment" to "neuron" (for interactions that transport its reactants across compartments, you will have to define two or more compartments, and assign them separately for elements (species) and the interaction (reaction))
- from "Reactants" drop down menu choose "SQSTM"
- from "Products" drop down menu choose "SQSTM1 (K13 ubiquitinated)"
- from "Modifiers" drop down menu choose "PRKN"
    - a dialog box will pop up to ask you for the relation type of the modifier, choose "Catalysis"
- Click on "Add reaction"

2.3.2 Note the updated view and the information banner below informing about creating the new interaction.

Also, that during the active curation mode, the left section informs about who else curates this fact. Check detailed [tutorial on group management](../configuration/configure.md#step-3-edit-group-details) for this.

<img src="../images/dnc_2_3_reaction.png"  width="600">

This fact is now available for all the members of this group. Please note that users with view capabilities will not be able to edit the fact.